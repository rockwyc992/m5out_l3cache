GEM5=./gem5/build/X86/gem5.opt
NV_CONFIG=./NVmain/Config/PCM_ISSCC_2012_4GB.config
TEST_PROJS=./test
MAIN_SE=src/l3se.py

function run {
    OUTDIR=$1
    shift

    ${GEM5} -r -e -d ${OUTDIR} \
        ${MAIN_SE} \
        -c ${TEST_PROJS} \
	--cpu-type=TimingSimpleCPU \
	--mem-type=NVMainMemory \
	--nvmain-config=${NV_CONFIG} \
	$@
}

L1CACHE="--caches --l1i_size=32kB --l1d_size=32kB"
L2CACHE="--l2cache --l2_size=128kB"
L3CACHE="--l3cache --l3_size=1MB"

#run output/m5out
run output/m5out_l1              ${L1CACHE}
run output/m5out_l2              ${L1CACHE} ${L2CACHE}
run output/m5out_l3_as2          ${L1CACHE} ${L2CACHE} ${L3CACHE} --l3_assoc=2
run output/m5out_l3_as16         ${L1CACHE} ${L2CACHE} ${L3CACHE}
run output/m5out_l3_as16384      ${L1CACHE} ${L2CACHE} ${L3CACHE} --l3_assoc=16384
run output/m5out_l3_rrip         ${L1CACHE} ${L2CACHE} ${L3CACHE} --l3_rrip
run output/m5out_l3_writeback    ${L1CACHE} ${L2CACHE} ${L3CACHE} --l3_assoc=4
run output/m5out_l3_writethrough ${L1CACHE} ${L2CACHE} ${L3CACHE} --l3_assoc=4 --writethrough

