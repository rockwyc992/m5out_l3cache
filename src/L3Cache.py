import m5
from m5.objects import Cache
from m5.objects import RRIPRP

class L3Cache(Cache):
    assoc = 16
    size = '16MB'
    tag_latency = 20 
    data_latency = 20 
    response_latency = 20
    mshrs = 512
    tgts_per_mshr = 20
    write_buffers = 256
    #replacement_policy = RRIPRP()
    #writeback_clean = True

    def __init__(self, opts=None):
        super(L3Cache, self).__init__()
        if not opts:
            return
        if opts.l3_size:
            self.size = opts.l3_size
        if opts.l3_assoc:
            self.assoc = opts.l3_assoc
        if opts.l3_rrip:
            self.replacement_policy = RRIPRP()
        if opts.writethrough:
            self.writeback_clean = True

    def connectCPUSideBus(self, bus):
        self.cpu_side = bus.master

    def connectMemSideBus(self, bus):
        self.mem_side = bus.slave
